package activity6;

public class BagelSandwich implements ISandwich{
    
    private String filling;

    public BagelSandwich()
    {
        this.filling = " ";
    }

    @Override
    public void addFilling(String topping)
    {
        this.filling += topping + " ";
    }

    @Override
    public String getFilling()
    {
        return this.filling;
    }

    @Override
    public boolean isVegetarian() {
        throw new UnsupportedOperationException();
    }
}
