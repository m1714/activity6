package activity6;

public class ChickenSandwich extends VegetarianSandwich{

    @Override
    public boolean isVegetarian()
    {
        return false;
    }

    @Override
    public String getProtein()
    {
        return "Chicken";
    }
    
}
